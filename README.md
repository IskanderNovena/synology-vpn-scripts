## Synopsis

Scripts to manage starting and stopping applications and Docker-containers when connecting
or disconnecting en OpenVPN-connection on a Synology Diskstation.  
Upon a (dis)connect, a message will be sent through PushBullet.

## Motivation

Because my first VPN-provider had somewhat unstable servers and the connection would drop
out quite regularly, I wanted to be sure that certain processes were no longer running.  
Synology DSM has no option to do this, other than through the OpenVPN connection-file with
calls to custom scripts. So... here we are :-)

## Installation

* Download and unzip or clone this project to a location the Synology. The location of git
  (when installed) is usually `/volume1/@appstore/git/bin/git` but could be installed on a
  different volume.
* Make sure that at least the administrators-group has Read/Write-access to the chosen
  location. The permission-tab for (shared) folders can work miracles here ;-)
* Check if the shell-scripts have the execute-bit set. If not, add it by executing the
  command `chmod +x *.sh` in the chosen location.

## Configuration

There are several .sample-files included.

### pushbullet.apikey.sample

- Create an Access Token on your [PushBullet settings-page][e1].  
- Rename `pushbullet.apikey.sample` to `pushbullet.apikey` and change the text in that
  file with the Access Token you created.  

### containers.conf.sample

- Rename `containers.conf.sample` to `containers.conf` and change the text in that file
  with a single name of a Docker-container per line.  
- Put them in the order you want the containers to be started. When stopping the
  containers, the reverse order will be used.  

### apps.conf.sample

- Rename `apps.conf.sample` to `apps.conf` and change the text in that file with a single
  name of an application per line.  
- Put them in the order you want the applications to be started. When stopping the
  applications, the reverse order will be used.  
  The name of an application might be found by retrieving a list of installed packages and
  the Package Control configuration-files. This can be done with the command
  `ll /etc/init/pkgctl-*`.  
  Another way is to let the Synology Package Manager tell you which applications it knows
  about with the command `/usr/syno/bin/synopkg list | awk '{print $1}'`.

### Configuring the OpenVPN-connection

Synology has a [how-to][e2] on how to configure the connection. Check the method
'To create an OpenVPN (via .ovpn) profile'.  
Before you import the ovpn-file, add the lines in the file `openvpn_config.add` to the
config. Make sure there are no duplicate entries, otherwise setting up the connection will
fail.  

### Preparing applications

If you don't want certain applications to start when the Diskstation reboots, stop them
through Package Center.  
When you do this, an override-file will be created in `/etc/init` which will prevent the
application from automatically starting.  
WARNING! Be sure to check running applications after Synology updates, especially the
bigger updates.  
Sometimes the override-files will be removed.  
As an example, for the application MariaDB, the override-file will be
`/etc/init/pkgctl-MariaDB.override`. This file will contain the text `manual`.

## Testing

To test if everything works as wanted, start and stop the VPN-connection and see if the
configured applications and/or containers are automatically started/stopped.  
For troubleshooting-purposes you might want to open a putty-session and tail the
openvpn.log file (`tail -f /var/log/openvpn.log') and see what messages appear.  

[e1]: https://www.pushbullet.com/#settings
[e2]: https://www.synology.com/en-global/knowledgebase/DSM/help/DSM/AdminCenter/connection_network_vpnclient
