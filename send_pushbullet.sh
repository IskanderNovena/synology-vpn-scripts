#!/bin/sh
# Determine the directory the script is located in
ScriptDir=$(dirname $(readlink -f $0))

while getopts a:c:s:t: option
do
        case "${option}"
        in
                a) APPLIST=${OPTARG};;
                c) CONTAINERLIST=${OPTARG};;
                s) Status=${OPTARG};;
                t) SleepTimer=${OPTARG};;
        esac
done


echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Preparing PushBullet-notification."

apps=(${APPLIST//,/ })
containers=(${CONTAINERLIST//,/ })

PushBulletBody="VPN is "$Status"!"
echo "SleepTimer is set to '"$SleepTimer"'"
IpAddress=$(curl http://v4.ifconfig.co/ip)

echo "Apps:"
echo ${apps[@]}
echo "Containers:"
echo ${containers[@]}

echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Determining application status."
# Query passed-on applications
for i in ${apps[@]}; do
    PushBulletBody+="\n"$(/usr/syno/bin/synopkg status ${i} | grep -v Status)
done

for i in ${containers[@]}; do
    PushBulletBody+="\n"${i}" is "$(docker inspect --format='{{.State.Status}}' ${i})
done

PushBulletBody+="\nCurrent IP-address is '"$IpAddress"'."

echo $PushBulletBody

# Get the PushBullet API-key which is stored in the file 'pushbullet.apikey'
pb_apikey=$(cat $ScriptDir/pushbullet.apikey)

# Send message to PushBullet, first wait to make sure a connection is available
sleep $SleepTimer
curl -u "$pb_apikey": --header 'Content-Type: application/json' --data-binary '{"type": "note", "title": "VPN is '"$Status"'! - '"$(date +'%Y-%m-%d %H:%M:%S')"'" , "body": "'"$PushBulletBody"'"}' 'https://api.pushbullet.com/v2/pushes'
