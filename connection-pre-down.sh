#!/bin/sh
# Determine the directory the script is located in
ScriptDir=$(dirname $(readlink -f $0))

echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Running pre-down script"

if [[ -e $ScriptDir/apps.conf ]]
then
    echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Stopping applications"
    # Make an array of the applications we want processed
    # declare -a apps
    # apps=()
    mapfile -t apps < $ScriptDir/apps.conf

    # Stop applications in order entered in array
    for i in ${apps[@]}; do
        /usr/syno/bin/synopkg stop ${i}
    done

    # Check again, stop them the hard way if needed
    for i in ${apps[@]}; do
        ps -ef | grep -E "(${i})" | grep -v grep | awk '{print $2}' | xargs --no-run-if-empty kill -15
        sleep 1
        ps -ef | grep -E "(${i})" | grep -v grep | awk '{print $2}' | xargs --no-run-if-empty kill -9
    done
fi

if [[ -e $ScriptDir/containers.conf ]]
then
    echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Stopping Docker containers"
    # Make an array of Docker-containers we want to process
    # Read the containers in reverse, to stop them in the 'correct' order
    declare -a containers=$(tac $ScriptDir/containers.conf)

    if [[ -n $containers ]]
    then
        # Stop the containers
        docker stop -t 0 ${containers[@]}
    fi

# Optionally, kill the containers with a bit more force
# docker kill --signal=9 ${containers[@]}
fi

# Timestamp when the connection went down
echo "["$(date +'%Y-%m-%d %H:%M:%S')"]" >> $ScriptDir/openvpn_conn_down

# Join the values to pass to the Send-Script
function join_by { local IFS="$1"; shift; echo "$*"; unset IFS; }

APPLIST=$(join_by , "${apps[@]}")
CONTAINERLIST=$(join_by , "${containers[@]}")

# Send message to PushBullet
nohup $ScriptDir/send_pushbullet.sh -s "DOWN" -t 15 -a "$APPLIST" -c "$CONTAINERLIST" &
disown -h
